public class ShirtTester {
    public static void main(String[] args)
    {
        Shirt myShirt= new Shirt();
        Shirt yourShirt=new Shirt();
        myShirt.price=560;
        myShirt.description="Pure Cotton";
        myShirt.quantityInStock=100;
        myShirt.colorCode='B';
        myShirt.displayShirtInformation();
        yourShirt.displayShirtInformation();
    }
}
